MOSH_VERSION = 1.2.4
MOSH_SITE = https://mosh.mit.edu/
MOSH_DEPENDENCIES = protobuf
MOSH_SOURCE = mosh-$(MOSH_VERSION).tar.gz 
# MOSH_AUTORECONF = YES
MOSH_GETTEXTIZE = YES
MOSH_CONF_OPTS = CPP="$(TARGET_CC) -E"
MOSH_WGET = --no-check-certificate

#HOST_MOSH_CONF_ENV = \ 
#	TARGET_CFLAGS="-march=c3" TARGET_CPPFLAGS="-march=c3"
#	TARGET_CPPFLAGS="$(TARGET_CPPFLAGS) -march=c3"

#MOSH_CONF_ENV = \
#	TARGET_CFLAGS="-march=c3" \
#	TARGET_CPPFLAGS="-march=c3"

MOSH_LICENSE = GPLv3+ 
MOSH_LICENSE_FILES = COPYING 
 
$(eval $(autotools-package))

