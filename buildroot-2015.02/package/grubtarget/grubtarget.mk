GRUBTARGET_VERSION = 1.99
# GRUBTARGET_SITE = $(BR2_GNU_MIRROR)/grub 
GRUBTARGET_SITE = http://ftp.gnu.org/pub/gnu/grub/
# grub2 requires flex 2.5.35 
GRUBTARGET_DEPENDENCIES = host-flex 
GRUBTARGET_SOURCE = grub-$(GRUBTARGET_VERSION).tar.gz 
# GRUBTARGET_AUTORECONF = YES
GRUBTARGET_GETTEXTIZE = YES
GRUBTARGET_CONF_OPTS = --disable-mm-debug --with-platform=pc --with-bootdir=/boot --with-grubdir=grub --disable-efiemu --disable-grub-emu-sdl --disable-grub-emu-usb --disable-grub-emu-pci --disable-grub-mkfont --disable-grub-mount --disable-device-mapper --disable-libzfs --program-prefix="" --disable-werror CPP="$(TARGET_CC) -E" --disable-grub-fstest 
# GRUBTARGET_CONF_ENV =

#HOST_GRUBTARGET_CONF_ENV = \
#        $(HOST_CONFIGURE_OPTS) \
#        CPP="$(HOSTCC) -E" \
#        TARGET_CC="$(TARGET_CC)" \
#        TARGET_CFLAGS="$(TARGET_CFLAGS)" \
#        TARGET_CPPFLAGS="$(TARGET_CPPFLAGS)"


#GRUBTARGET_CONF_OPTS = \
#        --target=$(GRUB2_TARGET) \
#        --with-platform=$(GRUB2_PLATFORM) \
#        --disable-grub-mkfont \
#        --enable-efiemu=no \
#        --enable-liblzma=no \
#        --enable-device-mapper=no \
#        --enable-libzfs=no \
#        --disable-werror

#HOST_GRUBTARGET_CONF_ENV = \ 
#	TARGET_CFLAGS="-march=c3" TARGET_CPPFLAGS="-march=c3"
#	TARGET_CPPFLAGS="$(TARGET_CPPFLAGS) -march=c3"

#GRUBTARGET_CONF_ENV = \
#	TARGET_CFLAGS="-march=c3" \
#	TARGET_CPPFLAGS="-march=c3"

GRUBTARGET_LICENSE = GPLv3+ 
GRUBTARGET_LICENSE_FILES = COPYING 
 
$(eval $(autotools-package))

