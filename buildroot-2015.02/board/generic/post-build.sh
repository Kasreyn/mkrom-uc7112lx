TARGETDIR=$1
BR_ROOT=$PWD
STR_INITTAB_SWAPON="swon:1:once:/sbin/swapon /swapfile"
STR_INITTAB_SWAPOFF="swof:6:wait:/sbin/swapoff -a"
# STR_INITTAB_REBOOT="reb2:6:wait:/sbin/reboot"
STR_INITTAB_REBOOT_1="reb1:6:wait:echo \"inittab: runlevel 6: sending all processes the KILL signal\""
STR_INITTAB_REBOOT_2="reb2:6:wait:kill -TERM -1"
STR_INITTAB_REBOOT_3="reb3:6:wait:sleep 5"
STR_INITTAB_REBOOT_4="reb4:6:wait:/sbin/reboot"

# buildroot post build script example
# http://cellux.github.io/articles/diy-linux-with-buildroot-part-2/

# create an empty /boot directory in target
# install -d -m 0755 $TARGETDIR/boot

# setup mount for /boot
# install -T -m 0644 $BR_ROOT/system/skeleton/etc/fstab $TARGETDIR/etc/fstab
# echo '/dev/mmcblk0p1 /boot vfat defaults 0 0' >> $TARGETDIR/etc/fstab

install -T -m 0644 $BR_ROOT/system/skeleton/etc/network/interfaces $TARGETDIR/etc/network/interfaces
echo 'auto eth0' >> $TARGETDIR/etc/network/interfaces
echo 'iface eth0 inet dhcp' >> $TARGETDIR/etc/network/interfaces
cp $BR_ROOT/.config $TARGETDIR/etc/buildroot.config

# mount from util-linux-2.23.2 skips swap (util-linux-2.20.1 uses a different mount.c) entries in fstab are ignored
# install -T -m 0644 $BR_ROOT/system/skeleton/etc/fstab $TARGETDIR/etc/fstab
# echo '/swapfile       swap            swap    defaults        0       0' >> $TARGETDIR/etc/fstab

dd if=/dev/zero of=$TARGETDIR/swapfile bs=1024 count=40000
sudo mkswap $TARGETDIR/swapfile
sudo chmod 600 $TARGETDIR/swapfile

if [ -e $TARGETDIR/etc/init.d/S01logging ]; then
	rm $TARGETDIR/etc/init.d/S01logging
fi

perl -pi.bak -e 's/^(sysl|klog|sklo|ssys|swap)/# $1/g' $TARGETDIR/etc/inittab

perl -pi.bak -e 's/^(root.*:).*/$1\/bin\/bash/g' $TARGETDIR/etc/passwd
perl -pi.bak -e 's/^root:[^:]+(.*)/root:eZAuttlJkCsOA$1/g' $TARGETDIR/etc/shadow
perl -pi.bak -e 's/^([^:]+:)\d+(:initdefault:).*/${1}1${2}/g' $TARGETDIR/etc/inittab

grep -q "$STR_INITTAB_SWAPON" $TARGETDIR/etc/inittab || echo $STR_INITTAB_SWAPON >> $TARGETDIR/etc/inittab
grep -q "$STR_INITTAB_SWAPOFF" $TARGETDIR/etc/inittab || echo $STR_INITTAB_SWAPOFF >> $TARGETDIR/etc/inittab
#grep -q "$STR_INITTAB_REBOOT" $TARGETDIR/etc/inittab || echo $STR_INITTAB_REBOOT >> $TARGETDIR/etc/inittab

grep -q "$STR_INITTAB_REBOOT_1" $TARGETDIR/etc/inittab || echo $STR_INITTAB_REBOOT_1 >> $TARGETDIR/etc/inittab
grep -q "$STR_INITTAB_REBOOT_2" $TARGETDIR/etc/inittab || echo $STR_INITTAB_REBOOT_2 >> $TARGETDIR/etc/inittab
grep -q "$STR_INITTAB_REBOOT_3" $TARGETDIR/etc/inittab || echo $STR_INITTAB_REBOOT_3 >> $TARGETDIR/etc/inittab
grep -q "$STR_INITTAB_REBOOT_4" $TARGETDIR/etc/inittab || echo $STR_INITTAB_REBOOT_4 >> $TARGETDIR/etc/inittab

# perl -pi.bak -e 's/^(rwmo)/# $1/g' $TARGETDIR/etc/inittab

mkdir $TARGETDIR/etc/ifplugd
install $BR_ROOT/board/generic/S49ntp $TARGETDIR/etc/init.d/
install $BR_ROOT/board/generic/ntp.conf $TARGETDIR/etc/
install $BR_ROOT/board/generic/.bashrc $TARGETDIR/root/
install $BR_ROOT/board/generic/.bash_aliases $TARGETDIR/root/
install $BR_ROOT/board/generic/shells $TARGETDIR/etc/
install $BR_ROOT/board/generic/ifplugd.action $TARGETDIR/etc/ifplugd/
install $BR_ROOT/board/generic/S40network $TARGETDIR/etc/init.d/

$BR_ROOT/board/generic/post-build-external.sh $1

