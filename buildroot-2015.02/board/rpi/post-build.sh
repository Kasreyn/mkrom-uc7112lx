TARGETDIR=$1
BR_ROOT=$PWD

$BR_ROOT/board/generic/post-build.sh $1

STR_INITTAB_GETTY_1="1:1:respawn:/sbin/getty 38400 tty1"
STR_INITTAB_GETTY_2="2:1:respawn:/sbin/getty 38400 tty2"

grep -q "$STR_INITTAB_GETTY_1" $TARGETDIR/etc/inittab || echo $STR_INITTAB_GETTY_1 >> $TARGETDIR/etc/inittab
grep -q "$STR_INITTAB_GETTY_2" $TARGETDIR/etc/inittab || echo $STR_INITTAB_GETTY_2 >> $TARGETDIR/etc/inittab

#S0::respawn:/sbin/getty -L  ttyS0 115200 vt100 # GENERIC_SERIAL
perl -pi.bak -e 's/^(.*)ttyS0(.*GENERIC_SERIAL)/$1ttyAMA0$2/g' $TARGETDIR/etc/inittab

install $BR_ROOT/board/rpi/interfaces $TARGETDIR/etc/network/

$BR_ROOT/board/rpi/post-build-external.sh $1
