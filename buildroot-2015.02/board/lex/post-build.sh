TARGETDIR=$1
BR_ROOT=$PWD

$BR_ROOT/board/generic/post-build.sh $1

STR_INITTAB_GETTY_1="1:1:respawn:/sbin/getty 38400 tty1"
STR_INITTAB_GETTY_2="2:1:respawn:/sbin/getty 38400 tty2"

grep -q "$STR_INITTAB_GETTY_1" $TARGETDIR/etc/inittab || echo $STR_INITTAB_GETTY_1 >> $TARGETDIR/etc/inittab
grep -q "$STR_INITTAB_GETTY_2" $TARGETDIR/etc/inittab || echo $STR_INITTAB_GETTY_2 >> $TARGETDIR/etc/inittab

install $BR_ROOT/board/lex/interfaces $TARGETDIR/etc/network/

$BR_ROOT/board/lex/post-build-external.sh $1
