TARGETDIR=$1
BR_ROOT=$PWD

$BR_ROOT/board/generic/post-build.sh $1

perl -pi.bak -e 's/^(\d.*getty)/# $1/g' $TARGETDIR/etc/inittab

install $BR_ROOT/board/moxart/S30setmac $TARGETDIR/etc/init.d/
install $BR_ROOT/board/moxart/UC-7112-LX-setmac $TARGETDIR/sbin/
install $BR_ROOT/board/moxart/interfaces $TARGETDIR/etc/network/

$BR_ROOT/board/moxart/post-build-external.sh $1
