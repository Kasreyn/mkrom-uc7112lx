#include <iostream>
#include <fstream>
#include <sstream>

int main(int argc, char* argv[]) {
	std::ofstream out_partition;
	std::ifstream in_binary;

	in_binary.open("zImage", std::ios::in|std::ios::binary);
	if (!in_binary) {
		std::cout << "Error: can not open zImage" << std::endl;
		return false;
	}

	out_partition.open("/dev/mtdblock1", std::ios::out|std::ios::binary);
	if (!out_partition) {
                std::cout << "Error: can not open mtd_file" << std::endl; 
                return false;
        }

	in_binary.seekg(0,std::ios::beg);
	out_partition << in_binary.rdbuf();
	in_binary.close();
	out_partition.close();
}

/*
 * /usr/local/arm-linux/bin/arm-linux-g++ -o kernel_upgrade_tool_uc7112 kernel_upgrade_tool_uc7112.cpp
 */
