
/*
 * This program to do Moxa IA200 series release ROM file. It will spild to 16M flash ROM.
 *
 * History:
 * Date		Author			Comment
 * 01-03-2006	Victor Yu.		Create it.
 * 					The flash ROM format is following:
 * 					ROM : offset 0 - 16M
 * 					0x000000 - 0x040000	Moxa proprity binary code
 * 					0x040000 - 0x200000	Linux kernel binay code
 * 					0x200000 - 0xa00000	Root file system.
 * 					0xa00000 - 0x1000000	user disk.
 */

#include	<stdio.h>
#include        <stdlib.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include        <fcntl.h>
#include        <errno.h>
#include        <termios.h>
#include        <time.h>
#include	<string.h>

#define MIN(a, b)	((a) > (b) ? (b) : (a))
#define MAX(a, b)	((a) > (b) ? (a) : (b))
#define BUFFER_SIZE	4096

typedef struct input_file_struct {
	char	sourcefile[128];	// the input file name
	int	needsize;		// ocuppy size on the flash ROM
	struct input_file_struct	*next;
} input_file_t;

static char		outputfilename[128];
static input_file_t	*head=NULL, *tail=NULL;
static char		buffer[BUFFER_SIZE];
static char		padbuffer[BUFFER_SIZE];

static void	parse_file(FILE *file)
{
	char		*ptr, fname[128], item[64];
	int		nsize;
	input_file_t	*now;

	while ( fgets(buffer, BUFFER_SIZE, file) ) {
		// first skip space character
		ptr = buffer;
		while ( *ptr && *ptr == ' ') ptr++;
		if ( !*ptr || *ptr == '#' )
			continue;

		// start to get the configuration parameter
		sscanf(ptr, "%s %s 0x%x", item, fname, &nsize);
		if ( strcmp(item, "output") == 0 ) {
			strcpy(outputfilename, fname);
			printf("The output file is [%s].\n", outputfilename);
		} else if ( strcmp(item, "input") == 0 ) {
			now = malloc(sizeof(input_file_t));
			if ( !now ) {
				printf("Allocate memory fail !\n");
				continue;
			}
			strcpy(now->sourcefile, fname);
			now->needsize = nsize;
			if ( !head )
				head = now;
			else
				tail->next = now;
			tail = now;
			tail->next = NULL;
			printf("The input file is [%s], need size = 0x%x\n", now->sourcefile, now->needsize);
		} else {
			printf("Unknow item : [%s]\n", item);
		}
	}
}

int main(int argc, char *argv[])
{
	int		ofd, ifd;
	FILE		*cfile;
	input_file_t	*now;

	if ( argc < 2  ) {
		printf("Usage: %s configuration-file-name\n", __FILE__);
		exit(1);
	}
	cfile = fopen(argv[1], "r");
	if ( cfile == NULL ) {
		printf("Open configuration file [%s] fail !\n", argv[1]);
		exit(1);
	}
	parse_file(cfile);
	fclose(cfile);

	ofd = open(outputfilename, O_RDWR|O_CREAT|O_TRUNC, 0666);
	if ( ofd < 0 ) {
		printf("Open output file [%s] fail !\n", outputfilename);
		exit(1);
	}

	memset(padbuffer, 0xff, BUFFER_SIZE);
	now = head;
	while ( now ) {
		int	rlen, nsize;

		ifd = open(now->sourcefile, O_RDONLY);
		if ( ifd < 0 ) {
			printf("Open input file [%s] fail !\n", now->sourcefile);
			break;
		}
		nsize = now->needsize;
		while ( nsize > 0 ) {
			rlen = MIN(nsize, BUFFER_SIZE);
			rlen = read(ifd, buffer, rlen);
			if ( rlen <= 0 ) {
				rlen = MIN(nsize, BUFFER_SIZE);
				write(ofd, padbuffer, rlen);
				nsize -= rlen;
				continue;
			}
			write(ofd, buffer, rlen);
			nsize -= rlen;
			if ( rlen < BUFFER_SIZE && nsize > 0 ) {	// need to pad size
				rlen = BUFFER_SIZE - rlen;
				rlen = MIN(rlen, nsize);
				write(ofd, padbuffer, rlen);
				nsize -= rlen;
			}
		}
		close(ifd);
		now = now->next;
	}
	close(ofd);
	exit(0);
}
