#!/bin/bash

if [ $# -eq 0 ]; then
	echo "usage: install_liveboot.sh [FILE]"
	exit 0
fi

DIR=/tmp/liveboot_root
if [ -d $DIR ]; then
	rm -r $DIR
fi
mkdir $DIR
unzip -d $DIR $1

if [ ! -d $DIR/system/su.d ]; then
	mkdir $DIR/system/su.d
fi

cat > $DIR/system/etc/init.d/99SuperSUDaemon << EOF
#!/system/bin/sh
/system/xbin/daemonsu --auto-daemon &
EOF

cat > $DIR/system/su.d/0000liveboot << EOF
#!/tmp-mksh/tmp-mksh
/tmp-mksh/tmp-mksh /system/bin/liveboot &
EOF
chmod 700 $DIR/system/su.d/0000liveboot 
# permissions are preserved in GNU zip/unzip
# somehow installing the zip (TWRP) changes the permissions
# mount /system and chmod 700 /system/su.d/0000liveboot with TWRP terminal

cat > $DIR/system/bin/liveboot << EOF
#!/tmp-mksh/tmp-mksh
toolbox cp /system/bin/app_process /dev/.app_process_liveboot
toolbox chown 0.0 /dev/.app_process_liveboot
toolbox chmod 0700 /dev/.app_process_liveboot
NO_ADDR_COMPAT_LAYOUT_FIXUP=1 ANDROID_ROOT=/system LD_LIBRARY_PATH=/vendor/lib:/system/lib CLASSPATH=/system/app/eu.chainfire.liveboot-1.apk /dev/.app_process_liveboot /system/bin eu.chainfire.liveboot.a.j /system/app/eu.chainfire.liveboot-1.apk /system/lib/libcfsurface.so boot dark logcatlevels=VDIWEFS logcatbuffers=MSC logcatformat=brief dmesg=0-99 lines=80 wordwrap save
toolbox rm /dev/.app_process_liveboot
EOF
#logcatlevels	= VDIWEFS	(Verbose Debug Info Warning Error Fatal Silent)
#logcatbuffers	= MSC		(Main Systen Crash)
#save				(saves log to /cache/liveboot.log)

cp daemonsu $DIR/system/xbin/
cp libcfsurface.so $DIR/system/lib/
cp apks/eu.chainfire.liveboot-1.apk $DIR/system/app/
cp /sda1/installfiles/android/apks/com.estrongs.android.pop-2.apk $DIR/system/app/
cp /sda1/installfiles/android/apks/mobi.infolife.taskmanager-3.1.9.apk $DIR/system/app/
cp /sda1/installfiles/android/apks/YouTube.v.10.37.58.b.107758133.crk.LVL.Auto.Removed.apk $DIR/system/app/
cp /sda1/installfiles/android/apks/at.knorre.vortex-1.apk $DIR/system/app/

#cp /sda1/backup/S4.backup.161010/clockworkmod/backup/2016-10-10.10.26.46/system/bin/su bin/
#cp /sda1/backup/S4.backup.161010/clockworkmod/backup/2016-10-31.14.20.32/system/xbin/su xbin/
#cp /sda1/backup/S4.backup.161010/clockworkmod/backup/2016-10-31.14.20.32/system/xbin/sugote xbin/
#cp /sda1/backup/S4.backup.161010/clockworkmod/backup/2016-10-31.14.20.32/system/xbin/sugote-mksh xbin/

cd $DIR
rm ../test.zip
zip -r ../test.zip *


