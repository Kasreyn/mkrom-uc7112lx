# TOOLCHAIN_SYSROOT=/home/i/x-tools/arm-unknown-linux-gnueabi_linux-2.6.19.7-09780ab-snapshot_eglibc-2.11_armv4/arm-unknown-linux-gnueabi/sysroot/
# KERNEL_ZIMAGE=/sda1/moxa_new_linux_kernel/git/10.0.1.70-linux-2.6.32.60-mach-moxart/arch/arm/boot/zImage
# KERNEL_ZIMAGE=/sda1/moxa_new_linux_kernel/linux-2.6.19.7-stable-09780ab-git-snapshot/arch/arm/boot/zImage
# KERNEL_ZIMAGE=/sda1/moxa_new_linux_kernel/git/linux-2.6.34.14-moxart/arch/arm/boot/zImage

KERNEL_PATH=/sda1/moxa_new_linux_kernel/git/torvalds-linux/
KERNEL_ZIMAGE=$(KERNEL_PATH)arch/arm/boot/zImage
KERNEL_DTB=$(KERNEL_PATH)arch/arm/boot/dts/moxart-uc7112lx.dtb

TOOLCHAIN_SYSROOT=/home/i/x-tools/arm-unknown-linux-gnueabi_linux-2.6.32.60-eglibc-2.11_armv4_v4bx/arm-unknown-linux-gnueabi/sysroot/
INIT_BIN=/sda1/moxa_new_linux_kernel/sysvinit-2.88dsf/src/init
BUSYBOX_SYSROOT=/sda1/src/busybox-git/_install
IPERF=/sda1/src/iperf-2.0.5/src/iperf
ETHTOOL=/sda1/src/ethtool-2.6.36/ethtool

all: uc_7112_lx_plus-1.2-1.4-botech.rom

mkrom:	mkrom.c
	gcc -DCONFIG_PRODUCT -DVERSIONPKG -m32 -o mkrom mkrom.c

# release: mkrom

mkrom-dbg:	mkrom.c
	gcc -DCONFIG_PRODUCT -DVERSIONPKG -DDEBUG -m32 -o mkrom-dbg mkrom.c

debug: mkrom-dbg

clean_mkrom:
	rm -f mkrom *.o mkrom-dbg

sysroot_target:
	-sudo rm -rf sysroot
	mkdir -p sysroot/sbin
#	cp -r $(TOOLCHAIN_SYSROOT)/lib* sysroot/
#	sudo cp -r $(TOOLCHAIN_SYSROOT)/sbin sysroot/
	sudo cp -r $(BUSYBOX_SYSROOT)/* sysroot/
	sudo cp $(IPERF) sysroot/bin/
	sudo cp $(ETHTOOL) sysroot/bin/
#	sudo rm sysroot/lib/*.a
#	sudo rm sysroot/lib/libstdc++*
#	sudo rm sysroot/sbin/init					# busybox makes the link in /sbin/ 
#	sudo cp $(INIT_BIN) sysroot/sbin/		
#	cd sysroot/bin/; sudo ln -s ../sbin/init init
	sudo cp -r sysroot_src/* sysroot/
#	cd sysroot/bin/; sudo ln -s busybox init	# not needed, only /sbin/init is

jffs2:
	sudo mkfs.jffs2 -l -e 0x20000 -d sysroot -o jffs2.dsk
	sudo cp jffs2.dsk /tftpboot/

linux_image_dtb:
	cat $(KERNEL_ZIMAGE) $(KERNEL_DTB) > zImage

linux_image_no_dtb:
	cp $(KERNEL_ZIMAGE) .

moxa_image:	linux_image_dtb
	./mkrom mkrom.conf

publish:
	sudo cp uc_7112_lx_plus-1.2-1.4-botech.rom /tftpboot/

uc_7112_lx_plus-1.2-1.4-botech.rom: sysroot_target jffs2 moxa_image publish

clean:
	rm uc_7112_lx_plus-1.2-1.4-botech.rom
	-sudo rm -rf sysroot


