#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
/*#include <cstdint> C++0x*/
#include <stdint.h>
#include <stdlib.h> 

/*
<0x80000050 0x6>;
<0x80000056 0x6>;
*/

int main(int argc, char* argv[]) {
	std::ifstream in_binary;
	std::vector<unsigned char> mac0, mac1;
	std::stringstream ss_mac0, ss_mac1;

	mac0.reserve(6);
	mac1.reserve(6);

	in_binary.open("/dev/mtdblock0", std::ios::in | std::ios::binary);
	if (!in_binary) {
		std::cout << "Error: can not open /dev/mtdblock0" << std::endl;
		return false;
	}
	in_binary.seekg(0x50,std::ios::beg);
	in_binary.read(reinterpret_cast<char *>(&mac0[0]), 6);
	in_binary.read(reinterpret_cast<char *>(&mac1[0]), 6);
	in_binary.close();

	std::cout << "eth0 address: ";	
	for (int i = 0; i < 6; i++) {
		ss_mac0 << std::hex << +static_cast<uint8_t>(mac0[i]);
		if (i < 5) ss_mac0 << ":";
	}
	std::cout << ss_mac0.str() << std::endl;

	std::cout << "eth1 address: ";
	for (int i = 0; i < 6; i++) {
		ss_mac1 << std::hex << +static_cast<uint8_t>(mac1[i]);
		if (i < 5) ss_mac1 << ":";
	}
	std::cout << ss_mac1.str() << std::endl;

	system(std::string("ifconfig eth0 hw ether " + ss_mac0.str()).c_str());
	system(std::string("ifconfig eth1 hw ether " + ss_mac1.str()).c_str());
}

/*
 * /usr/local/arm-linux/bin/arm-linux-g++ -o UC-7112-LX-setmac UC-7112-LX-setmac.cpp
 * g++ -o UC-7112-LX-setmac UC-7112-LX-setmac.cpp 
 */

