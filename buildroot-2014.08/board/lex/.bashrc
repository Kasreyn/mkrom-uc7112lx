# ~/.bashrc: executed by bash(1) for non-login interactive shells.

[ -z "$PS1" ] && return

shopt -s histappend
export PROMPT_COMMAND="history -a"

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

