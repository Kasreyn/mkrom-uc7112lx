GRUBT2_VERSION = 1.99
# GRUBT2_SITE = $(BR2_GNU_MIRROR)/grub 
GRUBT2_SITE = http://ftp.gnu.org/pub/gnu/grub/
# GRUBT2_SITE = http://git.savannah.gnu.org/cgit/grub.git/snapshot/
# GRUBT2_SITE = http://brn.ildjarn.com/debian/
# grub2 requires flex 2.5.35 
GRUBT2_DEPENDENCIES = host-flex 
GRUBT2_SOURCE = grub-$(GRUBT2_VERSION).tar.gz 
# GRUBT2_SOURCE = grub-2.02-beta2.tar.gz
# GRUBT2_SOURCE = grub2.tar.gz
# GRUBT2_AUTORECONF = YES
GRUBT2_GETTEXTIZE = YES
GRUBT2_CONF_OPTS = --disable-mm-debug --with-platform=pc --with-bootdir=/boot --with-grubdir=grub --disable-efiemu --disable-grub-emu-sdl --disable-grub-emu-usb --disable-grub-emu-pci --disable-grub-mkfont --disable-grub-mount --disable-device-mapper --disable-libzfs --program-prefix="" --disable-werror CPP="$(TARGET_CC) -E" --disable-grub-fstest 
# GRUBT2_CONF_ENV =

#HOST_GRUBT2_CONF_ENV = \
#        $(HOST_CONFIGURE_OPTS) \
#        CPP="$(HOSTCC) -E" \
#        TARGET_CC="$(TARGET_CC)" \
#        TARGET_CFLAGS="$(TARGET_CFLAGS)" \
#        TARGET_CPPFLAGS="$(TARGET_CPPFLAGS)"


#GRUBT2_CONF_OPTS = \
#        --target=$(GRUB2_TARGET) \
#        --with-platform=$(GRUB2_PLATFORM) \
#        --disable-grub-mkfont \
#        --enable-efiemu=no \
#        --enable-liblzma=no \
#        --enable-device-mapper=no \
#        --enable-libzfs=no \
#        --disable-werror

#HOST_GRUBT2_CONF_ENV = \ 
#	TARGET_CFLAGS="-march=c3" TARGET_CPPFLAGS="-march=c3"
#	TARGET_CPPFLAGS="$(TARGET_CPPFLAGS) -march=c3"

#GRUBT2_CONF_ENV = \
#	TARGET_CFLAGS="-march=c3" \
#	TARGET_CPPFLAGS="-march=c3"

GRUBT2_LICENSE = GPLv3+ 
GRUBT2_LICENSE_FILES = COPYING 
 
$(eval $(autotools-package))


