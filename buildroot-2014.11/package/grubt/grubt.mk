GRUBT_VERSION = 1.99
# GRUB2_SITE = $(BR2_GNU_MIRROR)/grub 
GRUBT_SITE = http://ftp.gnu.org/pub/gnu/grub/
# grub2 requires flex 2.5.35 
GRUBT_DEPENDENCIES = host-flex 
GRUBT_SOURCE  = grub-$(GRUB2_VERSION).tar.gz 
GRUBT_AUTORECONF = YES
GRUBT_GETTEXTIZE = YES
GRUBT_COMMON_CONF_OPT = --disable-mm-debug --with-platform=pc --with-bootdir=/boot --with-grubdir=grub --disable-efiemu --disable-grub-emu-sdl --disable-grub-emu-usb --disable-grub-emu-pci --disable-grub-mkfont --disable-grub-mount --disable-device-mapper --disable-libzfs
GRUBT_COMMON_CONF_OPT += --program-prefix="" 
GRUBT_COMMON_CONF_OPT += --disable-werror 
GRUBT_CONF_OPTS = $(GRUB2_COMMON_CONF_OPT) 
GRUBT_CONF_OPTS += CPP="$(TARGET_CC) -E" 
# GRUB2_CONF_ENV =
GRUBT_LICENSE = GPLv3+ 
GRUBT_LICENSE_FILES = COPYING 
 
$(eval $(autotools-package))

