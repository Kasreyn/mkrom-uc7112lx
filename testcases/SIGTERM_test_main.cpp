#include "SIGTERM_test_lib.hpp"

int main(int argc, char* argv[]) {
	Service s;
	s.InitSignals();

	while(s.ShouldBeAlive()) {
		usleep(1000000);
	}
}

/*

flags "-DIN_HPP -O2" are important, process exits normally without them (SIGTERM)

cross-g++ -DIN_HPP -O2 -shared -o libService.so SIGTERM_test_lib.cpp -lpthread; cross-g++ -DIN_HPP -O2 libService.so -o SIGTERM_test_main SIGTERM_test_main.cpp

scp SIGTERM_test_main libService.so root@10.0.1.218:/tmp/

cmd/stdout log:

# LD_LIBRARY_PATH=. ./SIGTERM_test_main &

Service::InitSignals() sigaction returned 0
Service::ShouldBeAlive() &g_sigTermSet=b6f5da98
Service::ShouldBeAlive() &g_sigTermSet=b6f5da98

# killall SIGTERM_test_main

SetSigTerm() sig=15 &g_sigTermSet=10a04
Service::ShouldBeAlive() &g_sigTermSet=b6f5da98
Service::ShouldBeAlive() &g_sigTermSet=b6f5da98

# killall SIGTERM_test_main

SetSigTerm() sig=15 &g_sigTermSet=10a04
Service::ShouldBeAlive() &g_sigTermSet=b6f5da98
Service::ShouldBeAlive() &g_sigTermSet=b6f5da98

# killall -9 SIGTERM_test_main
[1]+  Killed                     ./SIGTERM_test_main



/usr/bin/ld: /tmp/ccD5FXCh.o: relocation R_X86_64_32S against `SetSigTerm(int)' can not be used when making a shared object; recompile with -fPIC
g++ -DIN_HPP -fPIC -O2 -shared -o libService.so SIGTERM_test_lib.cpp -lpthread; g++ -DIN_HPP -O2 libService.so -o SIGTERM_test_main SIGTERM_test_main.cpp

*/
