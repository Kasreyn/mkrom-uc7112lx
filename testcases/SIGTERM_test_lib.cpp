#include "SIGTERM_test_lib.hpp"

#ifndef IN_HPP

volatile sig_atomic_t g_sigTermSet;

void SetSigTerm( int sig )
{      
        printf("SetSigTerm() sig=%d &g_sigTermSet=%x\n", sig, &g_sigTermSet);
        g_sigTermSet = 1 ;
}

#endif

bool Service::ShouldBeAlive() {
	printf("Service::ShouldBeAlive() &g_sigTermSet=%x\n", &g_sigTermSet);
	return (g_sigTermSet == 0) ;
}

void Service::InitSignals()  {
	int t;

	g_sigTermSet  = 0;
	sigfillset (&m_block_mask);
	m_usr_action.sa_handler   = SetSigTerm;
	m_usr_action.sa_mask      = m_block_mask;
	m_usr_action.sa_flags     = 0;
	t = sigaction( SIGTERM, &m_usr_action, NULL ) ;
	printf("Service::InitSignals() sigaction returned %d\n", t);
}

