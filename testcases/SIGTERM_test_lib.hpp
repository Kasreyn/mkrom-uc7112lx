#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#ifdef IN_HPP

volatile sig_atomic_t g_sigTermSet;

void SetSigTerm(int sig)
{      
        printf("SetSigTerm() sig=%d &g_sigTermSet=%x\n", sig, &g_sigTermSet);
        g_sigTermSet = 1;
}

#endif

class Service
{
public:
	bool			ShouldBeAlive();
	void			InitSignals();

	struct sigaction	m_usr_action;
	sigset_t		m_block_mask;
};
